use std::io::Write;
use std::process::{Command, Stdio};

#[test]
fn parses_numeric_example() {
    test_pipeline("1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet\n", "142\n");
}

#[test]
fn parses_alphanumeric_example() {
    test_pipeline("two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen", "281\n");
}

fn test_pipeline(input: &str, expected: &str) {
    let input = input.to_string();
    let path = env!("CARGO_BIN_EXE_day_1");

    let mut child = Command::new(path)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to spawn child process");

    let mut stdin = child.stdin.take().expect("Failed to open stdin");
    std::thread::spawn(move || {
        stdin
            .write_all(input.as_bytes())
            .expect("Failed to write to stdin");
    });

    let output = child.wait_with_output().expect("Failed to read stdout");
    assert_eq!(String::from_utf8_lossy(&output.stdout), expected);
}
