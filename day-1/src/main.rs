use std::io;
const DIGITS: &[&str; 10] = &["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
const DIGIT_WORDS: &[&str; 10] = &[
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

fn main() -> Result<(), std::io::Error> {
    let mut total: u32 = 0;

    loop {
        let mut line = String::new();

        let bytes = io::stdin().read_line(&mut line)?;
        if bytes == 0 {
            break;
        }

        let first_digit = get_digit(line.as_str(), true).unwrap();
        let last_digit = get_digit(line.as_str(), false).unwrap();

        total += first_digit * 10 + last_digit;
    }

    println!("{}", total);

    Ok(())
}

fn get_digit(input: &str, forwards: bool) -> Option<u32> {
    let find = if forwards { str::find } else { str::rfind };

    let mut found: Option<u32> = None;
    let mut found_pos: Option<usize> = None;

    for list in [DIGITS, DIGIT_WORDS] {
        for (ordinal, word) in list.iter().enumerate() {
            if let Some(pos) = find(input, word) {
                if found_pos == None
                    || forwards && pos < found_pos.unwrap()
                    || !forwards && pos > found_pos.unwrap()
                {
                    found_pos = Some(pos);
                    found = Some(ordinal.try_into().unwrap());
                }
            }
        }
    }
    found
}
